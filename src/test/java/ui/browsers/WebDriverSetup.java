package ui.browsers;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.Dimension;
import org.testng.annotations.BeforeSuite;
import static com.codeborne.selenide.Selenide.open;

public class WebDriverSetup {
    @BeforeSuite
    public void openChrome(){
        // Setting start URL
        open("https://www.zapatos.es/");
        WebDriverRunner.getWebDriver().manage().window().setSize(new Dimension(1920, 1080));
    }

    public void closeBrowser() {
        Selenide.closeWindow();
    }

}
