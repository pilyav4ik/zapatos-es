Feature: Cart functionality in Chrome

  Scenario Template: Add product to cart from product page in Chrome

    Then cart button must have text "Carrito de compra (0)"
    Given input "<search phrase>" to search
    And click search button
    Then click product with link title "<link title>"
    Then click button with text "Añadir al carrito"
    And select size <size>
    Then text with "El producto ha sido añadido al carrito" visible
    And text with "<tala>" in precart popup visible
    Then close popup window with added product
    When cart button must have text "Carrito de compra (1)"
    Then click cart button after added product
    Then click link with text "X Eliminar artículo"
    Then cart button must have text "Carrito de compra (0)"

    Examples:
      |search phrase|link title|size|tala|
      |zapatillas mayoral|Zapatillas MAYORAL - 47249 Rosa 73|37|Talla: 37|
